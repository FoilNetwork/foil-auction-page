export default {
  mutations: {
    SET_ASSET_KEY: 'SET_ASSET_KEY',
    SET_ASSET: 'SET_ASSET',
    SET_RATE: 'SET_RATE',
    SET_ORDERS: 'SET_ORDERS',
    SET_ORDERS_UNCONFIRMED: 'SET_ORDERS_UNCONFIRMED',
    UPDATE_TIMER: 'UPDATE_TIMER',
    SET_ERROR: 'SET_ERROR',
  },
  actions: {
    GET_ASSET: 'GET_ASSET',
    GET_ORDERS: 'GET_ORDERS',
    GET_ORDERS_UNCONFIRMED: 'GET_ORDERS_UNCONFIRMED',
    GET_RATE: 'GET_RATE',
    ADD_ERROR: 'ADD_ERROR',
  },
}
