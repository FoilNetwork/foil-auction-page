import {createStore} from 'vuex'
import types from './types'
import requests from '../service/requests'
import utils from '../utils'

export default createStore({
  state: {
    assetKey: null,
    asset: null,
    orders: [],
    ordersFoil: [],
    ordersDoge: [],
    ordersUnconfirmed: [],
    rates: null,
    timer: null,
    error: null,
  },
  getters: {
    ordersHigh(state) {
      return [
        ...state.ordersFoil.slice(0, 2),
        ...state.ordersDoge.slice(0, 2),
      ].map(utils.correctPropertiesOrder)
    },
    ordersUnconfirmed(state) {
      const sort = utils.sortOrders(state.ordersUnconfirmed).slice(0, 6)
      return sort.map(utils.correctPropertiesOrder)
    },
    rateFOIL(state) {
      return state.rates?.FOIL_USD?.last_price
        ? state.rates.FOIL_USD.last_price : 0
    },
    rateDOGE(state) {
      return state.rates?.DOGE_USD?.last_price
        ? state.rates.DOGE_USD.last_price : 0
    },
  },
  mutations: {
    [types.mutations.SET_ASSET_KEY](state, assetKey) {
      state.assetKey = +assetKey
    },
    [types.mutations.SET_ASSET](state, asset) {
      state.asset = asset
    },
    [types.mutations.SET_ORDERS](state, {orders, currency}) {
      switch (currency) {
        case 1:
          if (utils.isUpdateOrders(state.ordersFoil, orders)) {
            state.ordersFoil = orders
          }
          break
        case 18:
          if (utils.isUpdateOrders(state.ordersDoge, orders)) {
            state.ordersDoge = orders
          }
          break
        default:
          state.orders = orders
      }
    },
    [types.mutations.SET_ORDERS_UNCONFIRMED](state, ordersUnconfirmed) {
      if (utils.isUpdateOrders(state.ordersUnconfirmed, ordersUnconfirmed)) {
        state.ordersUnconfirmed = ordersUnconfirmed
      }
    },
    [types.mutations.SET_RATE](state, rates) {
      state.rates = rates
    },
    [types.mutations.UPDATE_TIMER](state) {
      state.timer = new Date().getTime()
    },
    [types.mutations.SET_ERROR](state, err) {
      state.error = err
    },
  },
  actions: {
    [types.actions.GET_ASSET]({commit, state, dispatch}) {
      requests.getAsset(state.assetKey)
        .then(async response => {
          commit(types.mutations.SET_ASSET, null)

          if (response.data.type_key !== 65) return

          const {name, creator, imageURL, imageType, imagePreviewMediaType} = response.data
          const person = await requests.getCreatorName(creator)
            .then(r => ({name: r.data.name, imageURL: r.data.imageURL}))
            .catch(() => ({}))

          commit(types.mutations.SET_ASSET, {
            name,
            creator,
            person,
            imageURL,
            imageType,
            imagePreviewMediaType
          })
        })
        .catch(err => {
          dispatch(types.actions.ADD_ERROR, err)
          commit(types.mutations.SET_ASSET, null)
        })
    },
    [types.actions.GET_ORDERS]({commit, state}, {have}) {
      requests.getOrders({have, want: state.assetKey})
        .then(response => {
          commit(types.mutations.SET_ORDERS, {
            orders: response.data.want,
            currency: have,
          })
        })
    },
    [types.actions.GET_ORDERS_UNCONFIRMED]({commit, state}) {
      requests.getUnconfirmed()
        .then(response => {
          commit(
            types.mutations.SET_ORDERS_UNCONFIRMED,
            response.data.filter(o => {
              return o.wantKey === state.assetKey && [1, 18].includes(o.haveKey)
            }),
          )
        })
    },
    [types.actions.GET_RATE]({commit}) {
      requests.getRate()
        .then(response => {
          commit(types.mutations.SET_RATE, response.data)
        })
    },
    [types.actions.ADD_ERROR]({commit}, err) {
      commit(types.mutations.SET_ERROR, err)
      setTimeout(() => commit(types.mutations.SET_ERROR, null), 5000)
    },
  },
})
