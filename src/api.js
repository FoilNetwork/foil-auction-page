import axios from 'axios'
import config from './config'

const instance = axios.create({
  baseURL: config.BASE_URL,
  timeout: 3000,
})

export default instance
