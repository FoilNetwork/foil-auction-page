import api from '../api'

export default {
  getAsset: assetKey => api.get(`/apiasset/${assetKey}`),
  getUnconfirmed: () => api.get('/api/tx/unconfirmed?type=50'),
  getRate: () => api.get('/apiexchange/spot/pairs'),
  getCreatorName: address => api.get(`/api/personbyaddress/${address}`),
  getOrders: (
    {
      have,
      want,
    },
  ) => api.get(`/apiexchange/ordersbook/${want}/${have}`),
}
