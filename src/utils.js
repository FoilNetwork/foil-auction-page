import config from './config'
import store from './store'

const currency = {1: 'FOIL', 18: 'DOGE'}

/**
 * Получение корректной ссылки на ноду
 * @param path
 * @returns {string}
 */
const correctURL = path => {
  return path.charAt(0) === '/' ? config.BASE_URL + path : path
}

/**
 * Получение корректных параметров ордера
 * @param order
 * @returns {{creator: string, seqNo: string, price: number}}
 */
const correctPropertiesOrder = order => ({
  id: order.signature ?? order.seqNo,
  creator: shortCreator(order.creator),
  price: Math.ceil(order.amountHave),
  priceUSD: calcPriceUSD(order),
  currency: currency[order.haveAssetKey || order.haveKey],
})

/**
 * Сосздание обрезанного адреса
 * @param creator
 * @returns {string}
 */
const shortCreator = creator => {
  return creator.substr(0, 8) + '...' + creator.substr(-8)
}

/**
 * Получение цены в USD
 * @param order
 * @returns {number}
 */
const calcPriceUSD = order => {
  const getter = `rate${currency[order.haveAssetKey ?? order.haveKey]}`
  const rate = store.getters[getter]
  return Math.ceil(rate * order.amountHave)
}

/**
 * Сортировка ордеров по стоимости
 * @param orders
 * @returns {*}
 */
const sortOrders = orders => {
  orders.sort((a, b) => +a.amountHave < +b.amountHave ? 1 : -1)
  return orders
}

/**
 * Поиск лучшей заявки
 * @param currency
 * @param confirmed
 * @param unconfirmed
 * @returns {*}
 */
const findBestPrice = (currency, confirmed, unconfirmed) => {
  const confirmedLocal = findBestPriceByCurrency(confirmed, currency)
  const unconfirmedLocal = findBestPriceByCurrency(unconfirmed, currency)
  return findBestPriceByType(confirmedLocal, unconfirmedLocal)
}

/**
 * Поиск лучшей заявки определенного токена
 * @param orders
 * @param currency
 * @returns {*}
 */
const findBestPriceByCurrency = (orders, currency) => {
  return orders.find(o => o.currency === currency)
}

/**
 * Поиск лучшей заявки из подтвержденных и неподтвержденных
 * @param confirmed
 * @param unconfirmed
 * @returns {*}
 */
const findBestPriceByType = (confirmed, unconfirmed) => {
  if (!confirmed?.price && !unconfirmed?.price) return null
  if (confirmed?.price && !unconfirmed?.price) return confirmed
  if (!confirmed?.price && unconfirmed?.price) return unconfirmed

  return confirmed.price > unconfirmed.price
    ? confirmed
    : unconfirmed
}

/**
 * Проверка, обновлять ордера или нет
 * @param orders
 * @param oldOrders
 * @returns {boolean}
 */
const isUpdateOrders = (orders, oldOrders) => {
  return JSON.stringify(orders) !== JSON.stringify(oldOrders)
}

export default {
  correctURL,
  correctPropertiesOrder,
  sortOrders,
  findBestPrice,
  findBestPriceByCurrency,
  findBestPriceByType,
  isUpdateOrders,
}
